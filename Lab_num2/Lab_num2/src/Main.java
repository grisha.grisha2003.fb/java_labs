import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Подстрока с уникальными символами: " + findMaxUniqueSubstr(in));

        int[] first = {3,7};
        int[] second = {4,8,11};
        int[] mergedArrays = mergeSortedArrays(first, second);

        int[] arrForMaxSubArr = {1, -2, 1,1,1, 5};
        //int[] arrForMaxSubArr = {-1, -2, -3,-4,-5, -6};
        System.out.println("Максимальная сумма подмассива: " + findMaxSumOfSubArray(arrForMaxSubArr));

        int[][] arrForRotation1 = {{1, 2, 3, 4}, {5,6,7,8}, {9,10,11,12}};
        arrForRotation1 = rotateAnArray1(arrForRotation1);

        int[] arrForFindingPair = {1,2,3,4,5};
        int[] pair = findPairForSum(arrForFindingPair, 10);

        int [][] arrForSum = {{1, 2, 3, 4}, {-1,-2,-3,-4}, {1,6,6,6}};
        int sum = countSumOfArray(arrForSum);

        int[][] arrForComparing = {{1, 2, 3, 4}, {5,6,7,8}, {9,10,11,12}};
        int[] maximumsOfEachStringOfArray = findMaxPerStr(arrForComparing);

        int[][] arrForRotation2 = {{1, 2, 3, 4}, {5,6,7,8}, {9,10,11,12}};
        arrForRotation2 = rotateAnArray2(arrForRotation2);

        }

    public static String findMaxUniqueSubstr(Scanner in)
    {
        String text = in.nextLine();
        String res = "";
        int beginIndex = 0, endIndex =0;
        Map<Character, Integer> alphabet = new HashMap<Character, Integer>();

        for(int i = 0; i < text.length(); i++)
        {
            char symbol = text.toCharArray()[i];
            if(alphabet.containsKey(symbol) && (alphabet.get(symbol) >= beginIndex))
            {
                String temp = text.substring(beginIndex, i);
                if(res.length() < temp.length()) res = temp;
                beginIndex = alphabet.get(symbol) + 1;
                alphabet.put(symbol, i);
            }
            else
            {
                alphabet.put(symbol, i);
            }
            if(i == text.length()-1)
            {
                String temp = text.substring(beginIndex, i + 1);
                if(res.length() < temp.length()) res = temp;
            }
        }
        return res;
    }

    public static int[] mergeSortedArrays(int[] first, int[] second)
    {
        int firstIter = 0, secondIter = 0;
        int[] res = new int[first.length + second.length];
        while((firstIter < first.length) && (secondIter < second.length))
        {
            if(first[firstIter] < second[secondIter])
            {
                res[firstIter + secondIter] = first[firstIter];
                firstIter++;
            }
            else
            {
                res[firstIter + secondIter] = second[secondIter];
                secondIter++;
            }
        }
        if(firstIter >= first.length)
        {
            while(secondIter < second.length)
            {
                res[firstIter + secondIter] = second[secondIter];
                secondIter++;
            }
        }
        else if(secondIter >= first.length)
        {
            while(firstIter < first.length)
            {
                res[firstIter + secondIter] = first[firstIter];
                firstIter++;
            }
        }
        return res;
    }

    public static int findMaxSumOfSubArray(int[] arr)
    {
        int res = Integer.MIN_VALUE;
        int currentSum = Integer.MIN_VALUE;
        for(int value : arr)
        {
            if(currentSum == Integer.MIN_VALUE)
                currentSum = value;
            if(value >= 0)
                currentSum += value;
            else
            {
                if(currentSum > res)
                    res = currentSum;
                if(currentSum >= -value)
                    currentSum += value;
                else
                    currentSum = Integer.MIN_VALUE;
            }
        }
        if(currentSum > res)
            res = currentSum;
        return res;
    }

    public static int[][] rotateAnArray1(int[][] arr)
    {
        int[][] res = new int[arr[0].length][arr.length];
        for(int i = arr.length - 1; i >= 0; i--)
            for(int j = 0; j < arr[0].length; j++)
                res[j][arr.length -1 - i] = arr[i][j];
        return res;
    }

    public static int[] findPairForSum(int[] arr, int target)
    {
        int[] res = null;
        for(int i = 0; i < arr.length; i++)
            for(int j = i+1; j < arr.length; j++)
                if( arr[i] + arr[j] == target)
                {
                    res = new int[] {arr[i], arr[j]};
                    return res;
                }
        return res;
    }

    public static int countSumOfArray(int [][] arr)
    {
        int res = 0;
        for(int[] littleArr : arr)
            for(int val : littleArr)
                res += val;
        return res;
    }

    public static int[] findMaxPerStr(int [][] arr)
    {
        int[] res = new int[arr.length];
        for(int i = 0; i < arr.length; i++)
        {
            res[i] = Integer.MIN_VALUE;
            for(int value : arr[i])
                if(res[i] < value) res[i] = value;
        }
        return res;
    }

    public static int[][] rotateAnArray2(int[][] arr)
    {
        int[][] res = new int[arr[0].length][arr.length];
        for(int i = 0; i < arr.length; i++)
            for(int j = arr[0].length - 1; j >= 0; j--)
                res[arr[0].length - 1 - j][i] = arr[i][j];
        return res;
    }
    }
