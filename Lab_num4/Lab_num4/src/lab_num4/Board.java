package lab_num4;

import lab_num4.Figures.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Board {

    HashMap<Coordinates, Figure> figures = new HashMap<>();

    public void setFigure(Figure figure, Coordinates coordinates){
        figure.Coordinates = coordinates;
        figures.put(coordinates, figure);
    }

    public void removeFigure(Coordinates coordinates){
        figures.remove(coordinates);
    }

    public void moveFigure(Move move){
        Figure figure = getFigure(move.from);
        removeFigure(move.from);
        setFigure(figure, move.to);
    }
    public boolean isSquareEmpty(Coordinates coordinates){
        return !figures.containsKey(coordinates);
    }

    public Figure getFigure (Coordinates coordinates){
        return figures.get(coordinates);
    }
    public void setupDefaultFiguresPositions()
    {
        for (int file = 1; file <= 8 ; file++)
        {
            setFigure(new Pawn(Color.WHITE, new Coordinates(file,2)), new Coordinates(file, 2));
            setFigure(new Pawn(Color.BLACK, new Coordinates(file,7)), new Coordinates(file, 7));
        }
        setFigure(new Rook(Color.WHITE, new Coordinates(1, 1)), new Coordinates(1, 1));
        setFigure(new Rook(Color.WHITE, new Coordinates(8, 1)), new Coordinates(8, 1));
        setFigure(new Rook(Color.BLACK, new Coordinates(1,8 )), new Coordinates(1, 8));
        setFigure(new Rook(Color.BLACK, new Coordinates(8,8 )), new Coordinates(8, 8));

        setFigure(new Knight(Color.WHITE, new Coordinates(2, 1)),new Coordinates(2, 1));
        setFigure(new Knight(Color.WHITE, new Coordinates(7, 1)),new Coordinates(7, 1));
        setFigure(new Knight(Color.BLACK, new Coordinates(2, 8)),new Coordinates(2, 8));
        setFigure(new Knight(Color.BLACK, new Coordinates(7, 8)),new Coordinates(7, 8));

        setFigure(new Bishop(Color.WHITE, new Coordinates(3, 1)), new Coordinates(3, 1));
        setFigure(new Bishop(Color.WHITE, new Coordinates(6, 1)), new Coordinates(6, 1));
        setFigure(new Bishop(Color.BLACK, new Coordinates(3, 8)), new Coordinates(3, 8));
        setFigure(new Bishop(Color.BLACK, new Coordinates(6, 8)), new Coordinates(6, 8));

        setFigure(new Queen(Color.WHITE, new Coordinates(4, 1)), new Coordinates(4, 1));
        setFigure(new Queen(Color.BLACK, new Coordinates(4,8)), new Coordinates(4, 8));

        setFigure(new King(Color.WHITE, new Coordinates(5, 1)), new Coordinates(5, 1));
        setFigure(new King(Color.BLACK, new Coordinates(5, 8)), new Coordinates(5, 8));
    }

    public static boolean isSquareBlack(Coordinates coordinates){
        return ((coordinates.file + coordinates.rank) % 2) == 0;
    }


    public boolean isSquareAttackedByColor(Coordinates coordinates, Color color) {

        List<Figure> oneColorFigures = getFiguresByColor(color);

        for (Figure figure : oneColorFigures){
            Set<Coordinates> attackedSquares = figure.getAvailableToAttackSquares(this);

            if(attackedSquares.contains(coordinates)){
                return true;
            }
        }
        return false;
    }

    private List<Figure> getFiguresByColor(Color color) {
        List<Figure> result = new ArrayList<>();
        for(Figure figure : figures.values()){
            if (figure.Color == color){
                result.add(figure);
            }
        }
        return result;
    }
}
