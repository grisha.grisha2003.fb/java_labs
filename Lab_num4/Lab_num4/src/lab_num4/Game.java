package lab_num4;
import java.util.Set;
import lab_num4.Figures.Figure;

public class Game {

    private final Board board;

    public Game(Board board) {
        this.board = board;
    }

    private  BoardConsoleRenderer renderer = new BoardConsoleRenderer();

    public void gameLoop(){

        boolean isWhiteToMove = true;
        while (true){
            renderer.render(board);

            Move move = InputCoordinates.inputMove(board, isWhiteToMove? Color.WHITE : Color.BLACK, renderer);
            board.moveFigure(move);

            isWhiteToMove = !isWhiteToMove;
        }
    }
}
