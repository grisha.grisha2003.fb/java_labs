package lab_num4.Figures;

import lab_num4.Board;
import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.HashSet;
import java.util.Set;

public class King extends Figure
{
    public King(lab_num4.Color color, lab_num4.Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getFigureMoves() {
        Set<CoordinatesShift> result = new HashSet<>();

        for(int fileShift = - 1; fileShift <= 1; fileShift++){
            for(int rankShift = -1; rankShift <=1; rankShift++){
                if(fileShift == 0 && rankShift == 0){
                    continue;
                }
                result.add(new CoordinatesShift(fileShift, rankShift));

            }
        }
        return result;
    }

    @Override
    protected boolean isSquareAvailableForMove(lab_num4.Coordinates coordinates, Board board) {
        boolean result = super.isSquareAvailableForMove(coordinates, board);

        if(result){
            return !board.isSquareAttackedByColor(coordinates,this.Color.opposite());
        }
        return false;
    }
}
