package lab_num4.Figures;

import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.Set;

public class Queen extends LongRangeFigure implements IBishop, IRook
{
    public Queen(lab_num4.Color color, lab_num4.Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getFigureMoves() {
        Set<CoordinatesShift> moves = getRookMoves();
        moves.addAll(getBishopMoves());
        return moves;
    }
}
