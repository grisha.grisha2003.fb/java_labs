package lab_num4.Figures;
import lab_num4.Coordinates;
import lab_num4.Color;
import java.util.HashSet;
import java.util.Set;

public class Bishop extends LongRangeFigure implements IBishop
{
    public Bishop(Color color, Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getFigureMoves() {
        return getBishopMoves();
    }

}
