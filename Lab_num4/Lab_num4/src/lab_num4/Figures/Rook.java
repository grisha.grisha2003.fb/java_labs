package lab_num4.Figures;

import lab_num4.Board;
import lab_num4.BoardUtils;
import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Rook extends LongRangeFigure implements IRook
{
    public Rook(lab_num4.Color color, lab_num4.Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getFigureMoves() {
        return getRookMoves();
    }
}
