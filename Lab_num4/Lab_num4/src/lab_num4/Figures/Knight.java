package lab_num4.Figures;

import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Knight extends Figure
{
    public Knight(lab_num4.Color color, lab_num4.Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getFigureMoves() {
        return new HashSet<>(Arrays.asList(
                new CoordinatesShift(1, 2),
                new CoordinatesShift(2, 1),
                new CoordinatesShift(1,-2),
                new CoordinatesShift(-2, 1),
                new CoordinatesShift(-1, 2),
                new CoordinatesShift(2,-1),
                new CoordinatesShift(-1, -2),
                new CoordinatesShift(-2, -1)
                ));
    }
}
