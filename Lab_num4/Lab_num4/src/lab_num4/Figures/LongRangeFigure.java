package lab_num4.Figures;

import lab_num4.Board;
import lab_num4.BoardUtils;
import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.List;

public abstract class LongRangeFigure extends Figure {
    public LongRangeFigure(Color color, Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected boolean isSquareAvailableForMove(Coordinates coordinates, Board board) {

        boolean result = super.isSquareAvailableForMove(coordinates, board);

        if(result){
            return isSquareAvailableForAttack(coordinates, board);
        } else {
            return false;
        }
    }

    @Override
    protected boolean isSquareAvailableForAttack(lab_num4.Coordinates coordinates, Board board) {
        List<Coordinates> coordinatesBetween;

        if(this.Coordinates.file == coordinates.file){
            coordinatesBetween = BoardUtils.getVerticalCoordinatesBetween(this.Coordinates, coordinates);
        }else if (this.Coordinates.rank == coordinates.rank){
            coordinatesBetween = BoardUtils.getHorizontalCoordinatesBetween(this.Coordinates, coordinates);
        }else{
            coordinatesBetween = BoardUtils.getDiagonalCoordinatesBetween(this.Coordinates, coordinates);
        }

        for(Coordinates c : coordinatesBetween) {
            if (!board.isSquareEmpty(c)) {
                return false;
            }
        }
        return true;
    }
}
