package lab_num4.Figures;

import lab_num4.Board;
import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.HashSet;
import java.util.Set;

public class Pawn extends Figure
{
    public Pawn(Color color, Coordinates coordinates) {
        super(color, coordinates);
    }

    @Override
    protected Set<CoordinatesShift> getFigureMoves() {
        Set<CoordinatesShift> result = new HashSet<>();

        if(this.Color == lab_num4.Color.WHITE){
            result.add(new CoordinatesShift(0,1));

            if(Coordinates.rank == 2){
                result.add(new CoordinatesShift(0, 2));
            }
            result.add(new CoordinatesShift(-1, 1));
            result.add(new CoordinatesShift(1,1));
        } else{
            result.add(new CoordinatesShift(0,-1));
            if(Coordinates.rank == 7){
                result.add(new CoordinatesShift(0, -2));
            }

            result.add(new CoordinatesShift(-1, -1));
            result.add(new CoordinatesShift(1,-1));
        }

        return result;
    }

    @Override
    protected boolean isSquareAvailableForMove(Coordinates coordinates, Board board) {
        if (this.Coordinates.file == coordinates.file){
            return board.isSquareEmpty(coordinates);

        }else {
            if (board.isSquareEmpty((coordinates))) {
                return false;
            } else {
                return board.getFigure(coordinates).Color != this.Color;
            }
        }
    }

    @Override
    protected Set<CoordinatesShift> getFigureAttacks() {
        Set<CoordinatesShift> result = new HashSet<>();

        if (this.Color == lab_num4.Color.WHITE) {
            result.add(new CoordinatesShift(-1, 1));
            result.add(new CoordinatesShift(1,1));
        } else {
            result.add(new CoordinatesShift(-1, -1));
            result.add(new CoordinatesShift(1,-1));
        }
        return result;
    }

}
