package lab_num4.Figures;

import lab_num4.Board;
import lab_num4.Color;
import lab_num4.Coordinates;

import java.util.HashSet;
import java.util.Set;

abstract public class Figure
{
    public final Color Color;
    public Coordinates Coordinates;

    public Figure(Color color, Coordinates coordinates) {
        Color = color;
        Coordinates = coordinates;
    }

    public Set<Coordinates> getAvailableMoveSquares(Board board){

        Set<Coordinates> result = new HashSet<>();

        for (CoordinatesShift shift : getFigureMoves()) {
            if (Coordinates.canShift(shift)){
                Coordinates newCoordinates = this.Coordinates.shift(shift);

                if (isSquareAvailableForMove(newCoordinates, board)){
                    result.add(newCoordinates);
                }
            }
        }
        return result;
    }

    protected boolean isSquareAvailableForMove(Coordinates coordinates, Board board){
        return board.isSquareEmpty(coordinates) || board.getFigure(coordinates).Color != Color;
    }

    protected  abstract Set<CoordinatesShift> getFigureMoves();

    protected Set<CoordinatesShift> getFigureAttacks() {
        return getFigureMoves();
    }


    public Set<Coordinates> getAvailableToAttackSquares(Board board) {
        Set<CoordinatesShift> figureAttacks = getFigureAttacks();
        Set<Coordinates> result = new HashSet<>();

        for(CoordinatesShift figureAttack : figureAttacks){
            if(this.Coordinates.canShift(figureAttack)){
                Coordinates shiftedCoordinates = this.Coordinates.shift(figureAttack);

                if(isSquareAvailableForAttack(shiftedCoordinates, board)){
                    result.add(shiftedCoordinates);
                }
            }
        }
        return result;
    }

    protected boolean isSquareAvailableForAttack(lab_num4.Coordinates shiftedCoordinates, Board board){
        return true;
    }


}
