package lab_num4;

import lab_num4.Board;
import lab_num4.Figures.CoordinatesShift;
import lab_num4.Figures.Figure;

import java.util.HashSet;
import java.util.Set;

public class Main
{
    public static void main(String[] args)
    {
        Board board = new Board();
        board.setupDefaultFiguresPositions();

        Game game = new Game(board);
        game.gameLoop();


    }

}