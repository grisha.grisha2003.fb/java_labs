package lab_num4;

import lab_num4.Figures.Figure;

import java.nio.channels.Pipe;
import java.util.Scanner;
import java.util.Set;

public class InputCoordinates {

    private static final Scanner scanner = new Scanner(System.in);

    public static Coordinates input(){
        while (true){
            System.out.println("Enter coordinates");

            String line = scanner.nextLine();

            if (line.length() != 2){
                System.out.println("Invalid format");
                continue;
            }

            char fileChar = line.charAt(0);
            char rankChar = line.charAt(1);

            if (!Character.isDigit(fileChar) || !Character.isDigit(rankChar)){
                System.out.println("Invalid format");
                continue;
            }

            int rank = Character.getNumericValue(rankChar);
            int file = Character.getNumericValue(fileChar);
            if (rank < 1 || rank > 8 || file < 1 || file > 8){
                System.out.println("Invalid format");
                continue;
            }
            return new Coordinates(file, rank);
        }
    }

    public static Coordinates inputFigureCoordinatesForColor(Color color, Board board){
        while (true) {
            Coordinates coordinates = input();

            if (board.isSquareEmpty(coordinates)) {
                System.out.println("Empty square");
                continue;
            }
            Figure figure = board.getFigure(coordinates);
            if (figure.Color != color){
                System.out.println("Not your turn");
                continue;
            }

            Set<Coordinates> availableMoveSquares = figure.getAvailableMoveSquares(board);
            if (availableMoveSquares.isEmpty()){
                System.out.println("No moves for this figure");
                continue;
            }
            return coordinates;
        }
    }

    public static Move inputMove(Board board, Color color, BoardConsoleRenderer renderer) {
        while (true) {
            Coordinates sourseCoordinates = InputCoordinates.inputFigureCoordinatesForColor(
                    color, board
            );

            Figure figure = board.getFigure(sourseCoordinates);
            Set<Coordinates> availableMoveSquares = figure.getAvailableMoveSquares(board);

            renderer.render(board, figure);
            Coordinates targetCoordinates = InputCoordinates.inAvailableSquare(availableMoveSquares);

            Move move = new Move(sourseCoordinates, targetCoordinates);

            if (isKingInCheckAfterMove(board, color, move)) {
                System.out.println("Check!");
                continue;
            }
            return move;
        }
    }

    public static Coordinates inAvailableSquare(Set<Coordinates> coordinates){
        while (true){
            Coordinates input = input();
            if (!coordinates.contains(input)){
                System.out.println("No moves for this figure");
                continue;
            }
            return  input;
        }
    }



}
