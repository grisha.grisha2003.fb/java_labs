package lab_num4;

import lab_num4.Figures.Figure;

import java.util.Set;

import static java.util.Collections.emptySet;

public class BoardConsoleRenderer {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_WHITE_FIGURE_COLOR = "\u001B[97m";
    public static final String ANSI_BLACK_FIGURE_COLOR = "\u001B[30m";
    public static final String ANSI_WHITE_SQUARE_COLOR = "\u001B[47m";
    public static final String ANSI_BLACK_SQUARE_COLOR = "\u001B[0;100m";
    public static final String ANSI_HIGHLIGHT_SQUARE_BACKGROUND = "\u001B[45m";

    public void render(Board board, Figure figureToMove) {
        Set<Coordinates> availableToMoveSquares = emptySet();

        if (figureToMove != null){
            availableToMoveSquares = figureToMove.getAvailableMoveSquares(board);
        }

        for (int rank = 8; rank >= 1; rank--) {
            String line = "";
            for (int file = 1; file <= 8; file++) {
                Coordinates coordinates = new Coordinates(file, rank);
                boolean isHighlight = availableToMoveSquares.contains(coordinates);

                if (board.isSquareEmpty(coordinates)) {
                    line += getSpriteForEmptySquare(coordinates, isHighlight);
                } else {
                    line += getFigureSprite(board.getFigure(coordinates), isHighlight);
                }


            }

            line += ANSI_RESET;
            System.out.println(line);
        }
    }
    public void render (Board board)
    {
        render(board, null);
    }

    private String colorizeSprite(String sprite, Color figureColor, boolean isSquareBlack, boolean isHighlighted) {

        String result = sprite;
        if (figureColor == Color.WHITE) {
            result = ANSI_WHITE_FIGURE_COLOR + result;
        } else {
            result = ANSI_BLACK_FIGURE_COLOR + result;
        }
        if (isHighlighted) {
            result = ANSI_HIGHLIGHT_SQUARE_BACKGROUND + result;
        }else if (isSquareBlack) {
            result = ANSI_BLACK_SQUARE_COLOR + result;
        } else {
            result = ANSI_WHITE_SQUARE_COLOR + result;
        }
        return result;
    }

    private String getSpriteForEmptySquare(Coordinates coordinates, boolean isHighlight) {

        return colorizeSprite("   ", Color.WHITE, Board.isSquareBlack(coordinates),isHighlight);
    }

    private String getFigureSprite(Figure figure, boolean isHighlight) {
        return colorizeSprite(" " + selectUnicodeSpriteForFigure(figure) + " ", figure.Color, Board.isSquareBlack(figure.Coordinates),isHighlight);
    }

    private String selectUnicodeSpriteForFigure(Figure figure){
        switch (figure.getClass().getSimpleName()){
            case "Pawn": return "P";
            case "Knight": return "K";
            case "Bishop": return "B";
            case "Rook": return "R";
            case "Queen": return "Q";
            case "King": return "M";
            default: return "";
        }
    }


}
