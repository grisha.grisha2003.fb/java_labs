package lab_num4;

public enum Color
{
    WHITE,
    BLACK;

    public Color opposite(){
        return this == WHITE ? BLACK : WHITE;
    }
}
