package lab_num4;

import lab_num4.Figures.CoordinatesShift;

import java.util.Objects;

public class Coordinates
{
    public final int rank;
    public final int file;


    public Coordinates(int file, int rank) {
        this.file = file;
        this.rank = rank;
    }

    public Coordinates shift(CoordinatesShift shift){
        return new Coordinates(this.file + shift.fileShift , this.rank + shift.rankShift);
    }

    public boolean canShift(CoordinatesShift shift){
        if (file + shift.fileShift < 1 || file + shift.fileShift > 8) return false;
        if (rank + shift.rankShift < 1 || rank + shift.rankShift > 8) return false;
        return true;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinates that = (Coordinates) o;
        return rank == that.rank && file == that.file;
    }

    @Override
    public int hashCode() {
        return Objects.hash(rank, file);
    }
}
