import java.util.Scanner;
import java.util.List;
import java.util.Collections;


public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        syracuseSequence(in);
        changingSignSum(in);
        findTreasure(in);
        logicMaxMin(in);
        twiceEven(in);

    }


    public static void syracuseSequence(Scanner in) {
        System.out.println("Задача №1. Ввод числа: ");
        int n = in.nextInt();

        int counter = 0;
        int temp = n;
        while (temp != 1) {
            if (temp % 2 == 0)
                temp /= 2;
            else
                temp = 3 * temp + 1;
            counter++;
        }
        System.out.println("Количество шагов =" + counter + "\n");
    }

    public static void changingSignSum(Scanner in)
    {
        System.out.println("Задача №2. Введите количество членов ряда:");
        int answer = 0;
        int countOfValues = in.nextInt();
        boolean isNegative = false;
        while(countOfValues > 0)
        {
            System.out.println("Введите член ряда: \t");
            answer += (isNegative ? -1 : 1) * in.nextInt();
            isNegative = !isNegative;
            countOfValues--;
        }
        System.out.println("Сумма:\t" + answer + "\n");
    }

    pulic static void findTreasure(Scanner in) {
        System.out.println("Задача №3 Введите координату Х клада. :");
        int xTreasure = in.nextInt();
        System.out.println("Введите координату Y клада:");
        int yTreasure = in.nextInt();

        int xCurrent = 0, yCurrent = 0, counter = -1;
        boolean treasureReached = false;
        String dir = "";
        int dist = 0;
        while (true) {
            if (!treasureReached) {
                switch (dir) {
                    case "север":
                        yCurrent += dist;
                        break;
                    case "юг":
                        yCurrent -= dist;
                        break;
                    case "запад":
                        xCurrent -= dist;
                        break;
                    case "восток":
                        xCurrent += dist;
                        break;
                    default:
                        break;
                }
                counter++;
            }
            if (xTreasure == xCurrent && yTreasure == yCurrent) treasureReached = true;
            System.out.println("Введите направление");
            in.nextLine();
            dir = in.nextLine();
            if (dir.equals("стоп")) break;
            System.out.println("Укажите расстояние");
            dist = in.nextInt();
        }
        System.out.println("Минимальное количество указаний карты:" + counter);
    }

        public static void logicMaxMin(Scanner in){

            nt rightRoad = 0, maxHeight = 0;
            System.out.println("Задача №4 Введите число дорог:");
            int numOfRoads = in.nextInt();
            for(int i = 0; i < numOfRoads; i ++)
            {
                System.out.println("Введите количество тунелей за " + (i+1) + " дорогу:");
                int tunnelsPerRoad = in.nextInt();
                System.out.println("Введите высоту(-ы) для  " + tunnelsPerRoad + " тунеля(-ей):");
                int maxHeightPerRoad = Integer.MAX_VALUE;
                while (tunnelsPerRoad > 0)
                {
                    int tempHeight = in.nextInt();
                    if(tempHeight < maxHeightPerRoad) maxHeightPerRoad = tempHeight;
                    tunnelsPerRoad--;
                }
                if(maxHeightPerRoad > maxHeight)
                {
                    maxHeight = maxHeightPerRoad;
                    rightRoad = i+1;
                }
            }
            System.out.println("Номер дороги: " + rightRoad + "\tНаибольшая высота грузовика: " + maxHeight);
    }
    public static void twiceEven(Scanner in)
    {
        System.out.println("Задача №5. Введите положительное трёхзначное число:");
        int number = in.nextInt();
        int sum = 0; int comp = 1;
        while(number > 0)
        {
            sum+= number % 10;
            comp *= number %10;
            number /= 10;
        }
        if((sum %2 == 0) && (comp % 2 == 0))
            System.out.println("Введёное число дважды чётное");
        else
            System.out.println("Введённое число не дважды чётное");
    }

}