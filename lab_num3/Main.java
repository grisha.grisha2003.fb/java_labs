import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        ArrayList<Cinema> cinemas = new ArrayList<>();
        boolean[][] seatsConfiguration =
                {{true,true,true,true,true}, {false,true,true,true,false}, {false, false, true, false, false}}   ;
        Hall hall = new Hall(seatsConfiguration);
        Cinema cinema = new Cinema(hall);
        AdminUI aui = new AdminUI(cinemas);
        aui.addCinema(cinema);
        aui.start();
        UserUI uui = new UserUI(cinemas);
        uui.start();
    }
}